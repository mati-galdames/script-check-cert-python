# Imagen base con Python
FROM python:3.9

# Directorio de trabajo dentro del contenedor
WORKDIR /app

# Copiar el script de Python y el requirements.txt al contenedor
COPY main.py .
COPY requirements.txt .
COPY check-cert.sh .

# Instalar las dependencias
RUN pip install --no-cache-dir -r requirements.txt

# Comando para ejecutar el script cuando se inicie el contenedor
CMD ["python", "main.py"]

