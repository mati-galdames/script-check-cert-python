import smtplib
import subprocess
from datetime import datetime, timedelta
from email.mime.text import MIMEText

# Configuración de la URL del sitio web y el correo electrónico
#URL_SITIO_WEB = "https://api-qa.chek.cl"
EMAIL_DESTINATARIO = "mati.galdamesc@gmail.com"
EMAIL_REMITENTE = "mati.galdames.gcp1@gmail.com"
EMAIL_CLAVE_REMITENTE = "sgmdgclkktgcsksw"


def enviar_notificacion(subject, body):
    try:
        msg = MIMEText(body)
        msg['Subject'] = subject
        msg['From'] = EMAIL_REMITENTE
        msg['To'] = EMAIL_DESTINATARIO

        smtpserver = smtplib.SMTP('smtp.gmail.com', 587)
        smtpserver.starttls()
        smtpserver.login(EMAIL_REMITENTE, EMAIL_CLAVE_REMITENTE)
        smtpserver.sendmail(EMAIL_REMITENTE, EMAIL_DESTINATARIO, msg.as_string())
        smtpserver.quit()
    except Exception as e:
        print("Error al enviar el correo:", e)



# Comando para ejecutar el archivo shell
comando = "./check-cert.sh"

# Ejecutar el archivo shell desde Python y capturar la salida
try:
    resultado = subprocess.check_output(comando, shell=True, text=True).strip()

    # Dependiendo del resultado del archivo .sh, realizar la notificación
    if resultado == "DAYS_90":
        enviar_notificacion("El certificado está próximo a expirar.","Faltan 90 días para la expiración, Favor revisar.")
    elif resultado == "DAYS_30":
        enviar_notificacion("El certificado está próximo a expirar","Faltan 30 días para la expiración, Favor revisar")
    elif resultado == "OK":
        enviar_notificacion("El certificado está dentro del rango permitido.", "El certificado esta sin problemas de expiración")
    else:
        print("Resultado desconocido:", resultado)

except subprocess.CalledProcessError as e:
    print(f"Ocurrió un error al ejecutar el archivo shell: {e}")
