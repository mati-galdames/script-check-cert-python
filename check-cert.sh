check_certificate_expiration() {
    local hostname="$1"
    local port="${2:-443}"
    local expire_date
    local days_left

    expire_date=$(openssl s_client -servername "$hostname" -connect "$hostname:$port" 2>/dev/null | openssl x509 -noout -enddate | awk -F'=' '{print $2}')
    current_date=$(date -u +"%b %d %T %Y GMT")

    expire_timestamp=$(date -u -d "$expire_date" +"%s")
    current_timestamp=$(date -u -d "$current_date" +"%s")
    
    difference=$((expire_timestamp - current_timestamp))
    days_difference=$((difference / 86400))

    if [ "$days_difference" -le 90 ]; then
        echo "DAYS_90"
    elif [ "$days_difference" -le 30 ]; then
	echo "DAYS_30"
    else
        echo "OK"
    fi
}

# Usage example
hostname="api-qa.chek.cl"
check_certificate_expiration "$hostname"

