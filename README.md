Este directorio contiene un programa en python que envia un correo de notificación de exipiración de certificado, envía una notificación cuado faltan 90 días para su expiración y cuando faltan 30 días.
 -    "main.py"     --> Contiene la función que envía la notificación, este utiliza el archivo "check-cert.sh" dentro de su codigo.
 - "check-cert.sh"  --> Realiza el escaneo del sitio para la revisión del certificado.
 -  "Dockerfile"    --> Para generar la imagen de Docker que realiza esta acción en caso sea necesario utilizarlo en docker.
 -  "main-cf.py"    --> Archivo que contiene el programa para ser utilizado desde una Cloud Functions de GCP y esta sea ejecutada a traves de un GET a la URL generada de la cloud functions, con esto se puede crear un CRON en Cloud Scheduler que apunte a la URL y este active la función. 
